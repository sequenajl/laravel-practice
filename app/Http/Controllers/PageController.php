<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
   
    public function hello() {
        $info = array(
            'topics' =>['HTML & CSS', 'JS', 'REACT']
        );
        return view('hello')->with($info);
    }

    public function about() {
        $titulo='About Page';
        return view('pages.about')->with('title', $titulo);
    }

    public function services() {
        $thisistheTitle = "Services Page";
        return view('pages.services')->with('serviceTitle', $thisistheTitle);
    }

    public function index() {
        $indexTitle = 'Welcome to Laravel';
        return view('pages.index')->with('index',$indexTitle);
    }
}

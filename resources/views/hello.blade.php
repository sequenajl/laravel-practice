@extends('layouts.app')

@section('content')
<div class="pt-3">
<h1>Hello from PageController</h1>

@if(count($topics) > 0)
    
    @foreach($topics as $topic)
        <ul>
            <li>
                {{$topic}}
            </li>
        </ul>
    @endforeach
    @else
    <h1>
        Nothing to display!
    </h1>

@endif
</div>

@endsection
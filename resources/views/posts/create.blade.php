@extends('layouts.app')

@section('content')
    <div class="pt-3">
        <h1>Create Blog Posts Here</h1>
            <form method="POST" action="/posts">
            @CSRF
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class ="form-control" placeholder="Title" name="title">
                </div>
                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea class ="form-control" name="body" rows="3" placeholder="Body"></textarea>
                </div>
                <button class="btn btn-info" type="submit">Submit</button>
            </form>
    </div>
    
@endsection
@extends('layouts.app')

@section('content')
<div class="pt-3">
    <h1>{{$posts->title}}</h1>
   <small>
       Written on {{$posts->created_at}}
   </small> 
   <hr>
   <p>{{$posts->body}}</p>
   <hr>
   <a href="/posts/{{$posts->id}}/edit" class="btn btn-success"> EDIT </a>
   <form method="POST" action="/posts/{{$posts->id}}">
       @csrf
       <input type="hidden" name="_method" value="DELETE">
       <button class="btn btn-danger">DELETE</button>
   </form>
</div>

@endsection
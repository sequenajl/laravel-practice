@extends('layouts.app')

@section('content')

    <div class="pt-3">
        <h1>Blog Posts</h1>
        @if(count($posts) > 0 )
            @foreach($posts as $post)
            <h3>
                <a href="/posts/{{$post->id}}">{{$post->title}}</a>
            </h3>
            <span>Written on {{$post->created_at}} </span>
            <hr>
            @endforeach
            {{$posts->links()}}
            
        @else
            <h3>No posts found</h3>
        @endif
        <a href="posts/create" class="btn btn-dark">Create Posts</a>
    </div>
@endsection
@extends('layouts.app')

@section('content')
<div class="pt-3">
    <h1> EDIT </h1>
    <form method="POST" action="/posts/{{$posts->id}}">
                @csrf
                <!-- spoofing -->
                <input type="hidden" name="_method" value="PATCH">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class ="form-control" value="{{$posts->title}}" name="title">
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea class ="form-control" name="body" rows="3">{{$posts->body}}</textarea>
                    </div>
                    <button class="btn btn-info" type="submit">Update</button>
    </form>
</div>
@endsection